"""
Test Suite
"""
from etl_demo.main import extract_data
from etl_demo.test.ibm_data import ibm_data


def test_extract_ok(mock_av_ibm_get):
    """
    Test Extract from Alpha Vantage Ok
    """
    result = extract_data("IBM")

    assert result == ibm_data
    assert mock_av_ibm_get["mock_ibm"].called
