"""
For Prefect Flows
"""
import prefect
from prefect import Flow, task
from prefect.run_configs import LocalRun
from prefect.storage import Local
from prefect.client import Client
from etl_demo.config import CONFIG
from etl_demo.main import extract_data


@task
def step_extract():
    """
    Fetch AV Stock Data
    """
    return extract_data("IBM", "full")


with Flow(
    "av-extract-data-single",
    run_config=LocalRun(
        labels=["dev", "stock-av"],
        env={
            "AV_API_KEY": CONFIG.av_api_key,
        },
    ),
    storage=Local(),
) as flow:
    step_a = step_extract()


if __name__ == "__main__":
    client = Client(
        api_server=CONFIG.prefect_api,
    )
    client.register(flow, project_name="default")
