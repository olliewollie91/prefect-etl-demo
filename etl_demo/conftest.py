"""
Fixtures
"""
import pytest
import respx
from httpx import Response

from etl_demo.main import AV_API_URL
from etl_demo.test.ibm_data import ibm_data


@pytest.fixture
def mock_av_ibm_get():
    """
    Mock AV Intraday Stock API for IBM
    """
    with respx.mock(
        base_url=AV_API_URL,
        assert_all_called=False,
    ) as respx_mock:
        mocked_route = respx_mock.get(
            "/query",
            name="mock_ibm",
            params={
                "function": "TIME_SERIES_INTRADAY",
                "symbol": "IBM",
                "interval": "5min",
                "apikey": "abc123",
                "outputsize": "compact",
            },
        )
        mocked_route.return_value = Response(
            200,
            json=ibm_data,
        )
        yield respx_mock
