"""
Config
"""
import os
from dataclasses import dataclass


@dataclass
class Config:
    """
    Config
    """

    av_api_key: str
    prefect_api: str

    @classmethod
    def create(cls):
        """
        Create a new Config
        """
        return cls(
            av_api_key=os.environ.get("AV_API_KEY"),
            prefect_api=os.environ.get("PREFECT_API"),
        )


CONFIG = Config.create()
