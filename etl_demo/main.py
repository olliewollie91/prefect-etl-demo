"""
Main Code
"""
import httpx
from loguru import logger

from etl_demo.config import CONFIG

AV_API_URL = "https://www.alphavantage.co"
DEFAULT_TIMEOUT = 10.0


def extract_data(symbol: str, output_size: str = "compact"):
    """
    Download intraday stock data from AlphaVantage
    """
    with httpx.Client(base_url=AV_API_URL, timeout=DEFAULT_TIMEOUT) as client:
        logger.info(f"Retrieving {symbol} with output: {output_size} ...")
        try:
            req = client.get(
                "/query",
                params={
                    "function": "TIME_SERIES_INTRADAY",
                    "symbol": symbol,
                    "interval": "5min",
                    "apikey": CONFIG.av_api_key,
                    "outputsize": output_size,
                },
            )
            result = req.json()
            logger.info(f"Retrieved {symbol}.")
            return result
        finally:
            client.close()
