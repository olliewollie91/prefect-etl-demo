# prefect-etl-demo

Prefect Data Scheduler ETL Demo

## Preparing Prefect

Requires: `docker` and `docker-compose`

### Install dependencies

```bash
python3 -m venv venv
source venv/bin/activate
(venv) pip install requirements.txt
# For Code Linting, running test cases...
(venv) pip install -r requirements-dev.txt
```

### Starting Prefect UI and API Server

```bash
prefect server start
```

This will start the UI and API Server services along with their backing resources

The UI for Prefect can be accessed at: `http://127.0.0.1:8080`

The API Server for Prefect can be accessed at: `http://127.0.0.1:4200/graphql`

### Running an agent

In order for tasks to be run in the Prefect Data Scheduler, we need to spawn agents that can accept tasks.

```bash
# In the same folder as this repo, same Python environment

source venv/bin/activate
(venv)$ prefect agent local start -l dev -l stock-av
```

### First Flow

Create the `default` project on Prefect
```bash
prefect create project default
```

Register our first flow with Prefect
```bash
PREFECT_API=http://127.0.0.1:4200 AV_API_KEY=abc123 PYTHONPATH=. python etl_demo/flow.py
```

## Contributing Guidelines

### Before submitting PR

#### Linting

```bash
pylint etl_demo
black etl_demo
isort etl_demo --profile black
```
